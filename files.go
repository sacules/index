package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/Sacules/go-taglib"
)

const (
	mp3  = ".mp3"
	flac = ".flac"
	aac  = ".m4a"
	ogg  = ".ogg"
)

// ReadTags retrieves metadata from the given taglib file.
func (track *TrackMetadata) ReadTags(file *taglib.File) {
	track.File = file

	track.Filename = file.Filename
	track.Title = file.Title()
	track.Artist = file.Artist()
	track.Release = file.Album()
	track.Genre = file.Genre()
	track.Year = file.Year()
	track.Track = file.Track()
	track.Comment = file.Comment()
	track.Length = file.Length()

	// Not implemented yet

	// track.AlbumArtist = file.AlbumArtist()
	// track.Country = file.Country()
	// track.TrackTotal = file.TrackTotal()
	// track.Disc = file.Disc()
	// track.DiscTotal = file.DiscTotal()
	// track.Publisher = file.Publisher()
	// track.Filename = file.Filename()
}

// WriteTags sets the internal taglib file's metadata and writes it to
// disk. Returns an error if it couldn't be saved properly.
func (track *TrackMetadata) WriteTags() error {
	track.File.SetTitle(track.Title)
	track.File.SetArtist(track.Artist)
	track.File.SetAlbum(track.Release)
	track.File.SetGenre(track.Genre)
	track.File.SetYear(track.Year)
	track.File.SetTrack(track.Track)
	track.File.SetComment(track.Comment)

	return track.File.Save()
}

// ReadFilesFromDir explores the given dir recursively, and send music files back.
func ReadFilesFromDir(root string, filech chan *taglib.File) {
	defer close(filech)

	err := filepath.Walk(root,
		func(path string, info os.FileInfo, err error) error {
			if !info.IsDir() && correctFormat(path) {
				file, err := taglib.Read(path)
				if err != nil {
					errch <- fmt.Errorf("error while reading file %s: %s", path, err)
				} else {
					filech <- file
				}
			}
			return err
		})

	if err != nil {
		errch <- fmt.Errorf("failed at walking the directory: %s", err)
	}
}

// ReadFilesFromShell takes standard input, reads the correct files, and
// sends them to the file channel.
func ReadFilesFromShell(filech chan *taglib.File) {
	defer close(filech)

	for _, path := range os.Args {
		if correctFormat(path) {
			file, err := taglib.Read(path)
			if err != nil {
				errch <- fmt.Errorf("error while reading file %s: %s", path, err)
			} else {
				filech <- file
			}
		}
	}
}

func correctFormat(path string) bool {
	if strings.HasSuffix(path, mp3) || strings.HasSuffix(path, flac) || strings.HasSuffix(path, ogg) {
		return true
	}

	if strings.HasSuffix(path, aac) {
		app.Stop()
		fmt.Printf("file %s has an invalid format\n", path)
		os.Exit(1)
	}

	return false
}
