package main

import (
	"fmt"
	"strconv"

	"github.com/Sacules/go-taglib"
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

const multval = "(mutiple values)"

// Label tags to print in screen.
var tags = [7]string{
	"Title",
	"Artist",
	"Release",
	"Genre",
	"Year",
	"Track",
	"Comment",

	// Not implemented yet.

	// "Album Artist",
	// "Country",
	// "Total Tracks",
	// "Disc",
	// "Total Discs",
	// "Publisher",
}

// ManualTag is a window that lets you edit the given tracks.
type ManualTag struct {
	Tracks  []*TrackMetadata
	Form    *tview.Form
	Fields  map[string]*tview.InputField
	Summary map[string]string
}

// NewManualTag creates a new Manual Tag window, ready with some empty
// fields to fill with track's metadata.
func NewManualTag() *ManualTag {
	manualTag := &ManualTag{
		Form:    tview.NewForm(),
		Fields:  make(map[string]*tview.InputField),
		Summary: make(map[string]string),
	}

	for _, tag := range tags {
		field := tview.NewInputField()
		field.SetLabel(tag)
		field.SetText("")

		manualTag.Fields[tag] = field
		manualTag.Form.AddFormItem(field)
	}

	manualTag.Form.AddButton("Save", func() {
		manualTag.CheckChanges()
		manualTag.Save()
	})
	manualTag.Form.AddButton("Cancel", func() {
		manualTag.Quit()
	})

	manualTag.Form.
		SetFieldBackgroundColor(tcell.ColorDarkBlue).
		SetButtonBackgroundColor(tcell.ColorDarkBlue)

	return manualTag
}

// Add the given track to the form.
func (manualTag *ManualTag) Add(track *TrackMetadata) {
	manualTag.Tracks = append(manualTag.Tracks, track)
}

// CheckChanges iterates through the Summary map and updates the track's
// corresponding metadata.
func (manualTag *ManualTag) CheckChanges() {
	for _, tag := range tags {
		fieldText := manualTag.Fields[tag].GetText()

		// Prevent the tags from being overwritten by "-"
		if fieldText == multval {
			continue
		}

		for _, track := range manualTag.Tracks {
			switch tag {
			case "Title":
				if fieldText != manualTag.Summary[tag] {
					track.Title = fieldText
				}

			case "Artist":
				if fieldText != manualTag.Summary[tag] {
					track.Artist = fieldText
				}

			case "Release":
				if fieldText != manualTag.Summary[tag] {
					track.Release = fieldText
				}

			case "Genre":
				if fieldText != manualTag.Summary[tag] {
					track.Genre = fieldText
				}

			case "Year":
				if fieldText != manualTag.Summary[tag] {
					value, err := strconv.Atoi(fieldText)
					if err == nil {
						track.Year = value
					}
				}

			case "Track":
				if fieldText != manualTag.Summary[tag] {
					value, err := strconv.Atoi(fieldText)
					if err == nil {
						track.Track = value
					}
				}

			case "Comment":
				if fieldText != manualTag.Summary[tag] {
					track.Comment = fieldText
				}

				// Not implemented yet.

				// case "Country":
				// 	if fieldText != manualTag.Summary[tag] {
				// 		track.Country = fieldText
				// 	}

				// case "Total Tracks":
				// 	if fieldText != manualTag.Summary[tag] {
				// 		value, err := strconv.Atoi(fieldText)
				// 		if err == nil {
				// 			track.TrackTotal = value
				// 		}
				// 	}

				// case "Disc":
				// 	if fieldText != manualTag.Summary[tag] {
				// 		value, err := strconv.Atoi(fieldText)
				// 		if err == nil {
				// 			track.Disc = value
				// 		}
				// 	}

				// case "Total Discs":
				// 	if fieldText != manualTag.Summary[tag] {
				// 		value, err := strconv.Atoi(fieldText)
				// 		if err == nil {
				// 			track.DiscTotal = value
				// 		}
				// 	}

				// case "Publisher":
				// 	if fieldText != manualTag.Summary[tag] {
				// 		track.Publisher = fieldText
				// 	}
			}
		}
	}
}

// Clear the form.
func (manualTag *ManualTag) Clear() {
}

// GetPrimitive returns the underflying Form primitive.
func (manualTag *ManualTag) GetPrimitive() tview.Primitive {
	return manualTag.Form
}

// Load songs into the Window.
func (manualTag *ManualTag) Load(filech chan *taglib.File) {
	msgch <- "Loading..."

	for file := range filech {
		track := new(TrackMetadata)
		track.ReadTags(file)
		file.Close()

		manualTag.Add(track)
	}

	msgch <- ""

	manualTag.Update()
	manualTag.Redraw()
}

// Quit the program.
func (manualTag *ManualTag) Quit() {
	app.Stop()
}

// Redraw the screen.
func (manualTag *ManualTag) Redraw() {
	app.Draw()
}

// Save the Tracks to file.
func (manualTag *ManualTag) Save() {
	var err error
	for _, track := range manualTag.Tracks {
		track.File, err = taglib.Read(track.Filename)
		if err != nil {
			errch <- fmt.Errorf("error reading %s: %s", track.Filename, err)
			return
		}

		err = track.WriteTags()
		if err != nil {
			errch <- fmt.Errorf("error writing tags for %s: %s", track.Filename, err)
			return
		}
	}

	msgch <- "[green]Changes have been saved."
}

// Update the form by parsing all of the tracks - this assumes
// they have all been loaded.
func (manualTag *ManualTag) Update() {
	for _, tag := range tags {
		for _, track := range manualTag.Tracks {
			i, ok := manualTag.Summary[tag]

			// Default value
			manualTag.Summary[tag] = multval

			switch tag {
			case "Title":
				if !ok || i == track.Title {
					manualTag.Summary[tag] = track.Title
				}
			case "Artist":
				if !ok || i == track.Artist {
					manualTag.Summary[tag] = track.Artist
				}
			case "Release":
				if !ok || i == track.Release {
					manualTag.Summary[tag] = track.Release
				}
			case "Genre":
				if !ok || i == track.Genre {
					manualTag.Summary[tag] = track.Genre
				}
			case "Year":
				if !ok || i == strconv.Itoa(track.Year) {
					manualTag.Summary[tag] = strconv.Itoa(track.Year)
				}
			case "Track":
				if !ok || i == strconv.Itoa(track.Track) {
					manualTag.Summary[tag] = strconv.Itoa(track.Track)
				}
			case "Comment":
				if !ok || i == track.Comment {
					manualTag.Summary[tag] = track.Comment
				}

				// Not implemented yet

				// case "Album Artist":
				// 	if !ok || i == track.AlbumArtist {
				// 		manualTag.Summary[tag] = track.AlbumArtist
				// 	}
				// case "Country":
				// 	if !ok || i == track.Country {
				// 		manualTag.Summary[tag] = track.Country
				// 	}
				// case "Total Tracks":
				// 	if !ok || i == strconv.Itoa(track.TrackTotal) {
				// 		manualTag.Summary[tag] = strconv.Itoa(track.TrackTotal)
				// 	}
				// case "Disc":
				// 	if !ok || i == strconv.Itoa(track.Disc) {
				// 		manualTag.Summary[tag] = strconv.Itoa(track.Disc)
				// 	}
				// case "Total Discs":
				// 	if !ok || i == strconv.Itoa(track.DiscTotal) {
				// 		manualTag.Summary[tag] = strconv.Itoa(track.DiscTotal)
				// 	}
				// case "Publisher":
				// 	if !ok || i == track.Publisher {
				// 		manualTag.Summary[tag] = track.Publisher
				// 	}
			}
		}

		field := manualTag.Fields[tag]
		field.SetText(manualTag.Summary[tag])
	}
}
