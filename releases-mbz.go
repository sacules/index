package main

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/michiwend/gomusicbrainz"
)

func mbClientInit() (*gomusicbrainz.WS2Client, error) {
	client, err := gomusicbrainz.NewWS2Client(
		"https://musicbrainz.org/ws/2",
		"A GoMusicBrainz example",
		"0.0.1-beta",
		"http://github.com/michiwend/gomusicbrainz")
	return client, err
}

func mbQueryRelease(client *gomusicbrainz.WS2Client, artist, release string, trackTotal, maxQueries int) (*gomusicbrainz.ReleaseSearchResponse, error) {
	// Avoid conflict when doing fuzzy searching
	artist = strings.Replace(artist, "~", "", -1)
	release = strings.Replace(release, "~", "", -1)

	var query string
	switch {
	case artist != "" && release != "":
		query = fmt.Sprintf(`artist:"%s~" AND release:"%s~"`, artist, release)

	case artist == "" && release == "":
		query = fmt.Sprintf(`artist:"Darude" AND release:"Sandstorm"`)

	case artist == "":
		query = fmt.Sprintf(`release:"%s~"`, release)

	case release == "":
		query = fmt.Sprintf(`artist:"%s~"`, artist)
	}

	if trackTotal > 0 {
		query = fmt.Sprintf("%s AND tracks:%d", query, trackTotal)
	}

	return client.SearchRelease(query, maxQueries, -1)
}

func mbGetRelease(release *gomusicbrainz.Release, releasech chan *ReleaseMetadata) {
	// Normal metadata,
	newrelease := &ReleaseMetadata{
		Provider:      "MusicBrainz",
		Release:       release.Title,
		Country:       release.CountryCode,
		Type:          release.ReleaseGroup.Type,
		MusicBrainzID: release.ID}

	// Edge cases / weird stuff
	if release.Date.Year() != 1 {
		newrelease.Year = release.Date.Year()
	}

	for _, medium := range release.Mediums {
		newrelease.Medium += medium.Format + " "
	}

	for _, label := range release.LabelInfos {
		// For some reasons sometimes there's a NULL label?
		if label.Label != nil {
			newrelease.Publisher += label.Label.Name + ", "
		}
	}
	newrelease.Publisher = strings.TrimSpace(newrelease.Publisher)

	for _, credit := range release.ArtistCredit.NameCredits {
		newrelease.Artist += credit.Artist.Name + "; "
	}
	newrelease.Artist = strings.TrimRight(newrelease.Artist, "; ")

	releasech <- newrelease
}

// MBSearchRelease is called by goindex to retrieve info about available
// releases. Returns all it finds to the release channel
func MBSearchRelease(artist, release string, trackTotal, maxQueries int, releasech chan *ReleaseMetadata, wg *sync.WaitGroup) {
	// Ensure this gets out of the wait group
	defer wg.Done()

	// Start client to connect to MusicBrainz
	client, err := mbClientInit()
	if err != nil {
		errch <- fmt.Errorf("error while generating MusicBrainz client: %s", err)
		return
	}

	// Send query
	resp, err := mbQueryRelease(client, artist, release, trackTotal, maxQueries)
	if err != nil {
		errch <- fmt.Errorf("error while querying MusicBrainz database: %s", err)
		return
	}

	// Get releases and send them back
	for _, release := range resp.Releases {
		mbGetRelease(release, releasech)
	}
}

// MBSearchTracklist is idk
func MBSearchTracklist(release *ReleaseMetadata) []*RecordMetadata {
	client, err := mbClientInit()
	if err != nil {
		errch <- fmt.Errorf("error while generating MusicBrainz client: %s", err)
		return nil
	}

	mbrelease, err := client.LookupRelease(release.MusicBrainzID, "recordings")
	if err != nil {
		errch <- fmt.Errorf("error while looking up the tracklist: %s", err)
		return nil
	}

	recordList := make([]*RecordMetadata, 0)
	for _, medium := range mbrelease.Mediums {
		record := &RecordMetadata{
			Format:   medium.Format,
			Position: medium.Position,
		}
		for _, mbtrack := range medium.Tracks {
			track := &TrackMetadata{
				Title:       mbtrack.Recording.Title,
				Artist:      release.Artist,
				AlbumArtist: release.Artist,
				Release:     release.Release,
				Year:        release.Year,
				Track:       mbtrack.Position,
				Comment:     mbrelease.Disambiguation,
				Length:      time.Duration(mbtrack.Length) * time.Millisecond,

				// Not implemented yet

				// Country:     release.Country,
				// TrackTotal:  len(medium.Tracks),
				// Disc:        medium.Position,
				// DiscTotal:   len(mbrelease.Mediums),
				// Publisher:   release.Publisher,
			}
			record.Tracks = append(record.Tracks, track)
		}
		recordList = append(recordList, record)
	}

	return recordList
}
